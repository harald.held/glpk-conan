from conans import ConanFile, AutoToolsBuildEnvironment, tools
from conans.errors import ConanInvalidConfiguration


class GlpkConan(ConanFile):
    name = "glpk"
    version = "4.65"
    license = "GPL-3.0-only"
    author = "Harald Held (harald.held@gmail.com)"
    url = "https://gitlab.com/harald.held/glpk-conan"
    description = """The GLPK (GNU Linear Programming Kit) package is intended for solving large-scale linear programming (LP),
                     mixed integer programming (MIP), and other related problems. It is a set of routines written in ANSI C and
                     organized in the form of a callable library.
                     See https://www.gnu.org/software/glpk/ for details"""
    topics = ("LP", "MILP", "linear programming", "integer programming")
    settings = "os", "compiler", "build_type", "arch"

    def configure(self):
        if self.settings.os != "Linux":
            raise ConanInvalidConfiguration("Currently, this recipe is only supported for Linux")

    def source(self):
        tools.get("http://ftp.gnu.org/gnu/glpk/glpk-4.65.tar.gz")

    def build(self):
        with tools.chdir("./glpk-4.65"):
            autotools = AutoToolsBuildEnvironment(self)
            autotools.fpic = True
            autotools.configure()
            autotools.make()

    def package(self):
        self.copy("glpk.h", dst="include", src="glpk-4.65/src", keep_path=False)
        self.copy("*.so.*", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["glpk"]
